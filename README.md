# tw2html

Checks online status of streams on twitch.tv and lets you watch them right here!


## Usage:

You can simply call the `?streams` endpoint with a comma-separated list of your favorite streams:

* index.php?streams=gamingonlinux,linustechtips,...

Assuming you host this service under the URL "https://tw2html.example.com", you can open up the streamlist like so:

```
https://tw2html.example.com/?streams=gamingonlinux,linustechtips
```

The site will show you a loading screen, while the status of streams are fetched in the background. Once the process is finished, the site refreshes by itself and shows you a list of online streams. You can watch them directly on tw2html via iframe-embedding (this transfers your IP and user-agent information to twitch.tv). Alternatively if your client can handle `streamlink://` schemes, you may open the stream via [streamlink](https://streamlink.github.io/) as well.

## Requirements:

- PHP-7.2 or higher

## Further notes

### streamlink:// scheme handler

tw2html offers buttons with the custom `streamlink://` scheme. It gives you the opportunity to open up streams on your computer via streamlink.

**For Mozilla Firefox:**

Add following key in [`about:config`](about:config):

- Key: `network.protocol-handler.expose.streamlink`
- Type: Boolean
- Value: `true`

**For GNU/Linux:**

In addition to the scheme handler in your browser (see above for Firefox), you need to add a streamlink-handler (e.g. in `~/.local/bin/streamlink-handler.sh`), such as:

```bash
#!/usr/bin/env bash

function urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

URL=$(urldecode "$1" | sed -E -e 's/\?.*$//' -e 's/^streamlink/https/')
TITLE=$(urldecode "$1" | sed -E -e 's/^.*?title=//')

streamlink \
  --player "mpv" \
  --title "${TITLE}" \
  "${URL}" best
```

and the according `.desktop` file (e.g. in `~/.local/share/applications/streamlink-handler.desktop`):

```
[Desktop Entry]
Type=Application
Name=Streamlink Scheme Handler
Exec=.local/bin/streamlink-handler.sh %u
StartupNotify=false
NoDisplay=true
MimeType=x-scheme-handler/streamlink;
```

Afterwards, add the new scheme handler to xdg:
```bash
xdg-mime default streamlink-handler.desktop x-scheme-handler/streamlink
```
