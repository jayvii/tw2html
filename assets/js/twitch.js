function toggle_player(stream, img64) {
	var img = document.querySelector("#img-" + stream);
	var ply = document.querySelector("#play-" + stream);
	if (img != null) {
		var player = document.createElement("iframe");
		player.allowFullscreen = "true";
		player.width = "100%";
		player.height = "100%";
		player.frameborder = "0";
		player.scrolling = "no";
		player.src = "https://player.twitch.tv/?channel=" +
			stream +
			"&parent=" +
			window.location.hostname;
		player.id = "play-" + stream;
		player.classList.add("preview");
		img.replaceWith(player);
	}
	if (ply != null) {
		var image = document.createElement("img");
		image.src = img64;
		image.id = "img-" + stream;
		image.classList.add("preview");
		ply.replaceWith(image);
	}
}
function toggle_chat(stream) {
	var plho = document.querySelector("#chat-placeholder-" + stream);
	var chat = document.querySelector("#chat-" + stream);
	if (chat != null) {
		var placeholder = document.createElement("div");
		placeholder.id = "chat-placeholder-" + stream;
		chat.replaceWith(placeholder);
	}
	if (plho != null) {
		var chatembed = document.createElement("iframe");
		chatembed.width = "100%";
		chatembed.height = "100vh";
		chatembed.frameborder = "0";
		chatembed.scrolling = "auto";
		chatembed.src = "https://www.twitch.tv/embed/" +
			stream +
			"/chat?parent=" +
			window.location.hostname;
		chatembed.id = "chat-" + stream;
		chatembed.classList.add("preview"); /* FIXME: wrong class */
		plho.replaceWith(chatembed);
	}
}

