<!DOCTYPE HTML>
<html>
<head>
	<title>Twitch Streams</title>
	<link rel="icon" type="image/png" href="/assets/img/twitch.png">
	<link crossorigin="use-credentials" rel="manifest" href="/manifest.json">
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="/assets/css/miniport.css" />
	<link rel="stylesheet" href="/assets/css/custom.css" />
	<link rel="stylesheet" href="/assets/css/loading.css" />
	<script async src="/assets/js/twitch.js"></script>
</head>

<?php

// Start timer
$time0 = time();

// Fetch GET and POST arguments
if (is_null($_POST["streams"])) {
	$loading_screen = true;
	if (!is_null($_GET["streams"])) {
		$channels = explode(",", $_GET["streams"]);
	} else {
		$channels = explode(",", $_COOKIE["streams"]);
	}
} else {
	$loading_screen = false;
	$channels = unserialize($_POST["streams"]);
}

if (is_null($_GET["stream"])) {
	$extplayer = false;
} else {
	$extplayer = true;
}

// Prepare streams array
$streams = array(
	"stream" => $channels,
	"desc" => array(),
	"game" => array(),
	"status" => array(),
	"img" => array(),
	"time" => array()
);

// Check whether to load streams or show the loading screen
if (!$loading_screen) {

	// Check Online Status of streams
	for ($i = 0; $i < sizeof($streams["stream"]); $i++) {

		// Fetch Stream Preview Image
		$img = file_get_contents(
			"https://static-cdn.jtvnw.net/previews-ttv/live_user_" .
			$streams["stream"][$i] .
			"-853x480.jpg"
		);
		$img_base64 = "data:image/jpg;base64," . base64_encode($img);

		// Check Online-Status via HTTP status code
		$http_code = preg_replace(
			'/^.*(\d{3}).*/',
			'\1',
			$http_response_header[0]
		);

		if (!($http_code == "200")) {
			
			// Stream is not online!
			array_push($streams["status"], 0);
			$desc = "";
			$game = "";
			$img_base64 = "";

		} else {

			// Stream is online!
			array_push($streams["status"], 1);

			// Fetch HTML
			$html = file_get_contents(
				"https://m.twitch.tv/" . $streams["stream"][$i]
			);
			$html = str_replace(array("\r", "\n"), "", $html);

			// Parse Game Name
			$pattern = '/^.*<p.*?Playing <a.*?>(.*?)<\/a>.*$/';
			$replacement = '\1';
			$game = substr(
				strip_tags(
					preg_replace(
						$pattern,
						$replacement,
						$html
					)
				),
				0,
				100
			);

			// Parse Stream Description
			$html = preg_replace('/<script.*<\/script>/', '', $html);
			$html = explode(">", $html);
			$desc_raw = implode("", preg_grep('/name="description"/', $html));
			$pattern = '/^.*content="(.*?)".*$/';
			$replacement = '\1';
			$desc = substr(
				strip_tags(
					preg_replace(
						$pattern,
						$replacement,
						$desc_raw
					)
				),
				0,
				500
			);

		}

		// Fill Streams array
		array_push($streams["desc"], $desc);
		array_push($streams["game"], $game);
		array_push($streams["img"], $img_base64);
		array_push($streams["time"], (time() - $time0));

	}

	// Generate Main Menu
	if (array_sum($streams["status"]) > 0) {
		$beep_color = "#0f0";
		$beep_class = "pulse";
	} else {
		$beep_color = "#f00";
		$beep_class = "nopulse";
	}
	$menu_items = "<li><strong style='color:#fff;'>" .
		"<span style='color:" . $beep_color .
		"' class='" . $beep_class . "'>•</span>" .
		"Live: " . array_sum($streams["status"]) . "</strong>" .
		"</li>";

	// Generate Content List
	$content = "";
	for ($i = 0; $i < sizeof($streams["stream"]); $i++) {
		if ($streams["status"][$i] > 0) {
			$content = $content .
				// Stream-Channel
				"<div id='" . $streams["stream"][$i] . "'></div>" .
				// Headline
				"<h2>" . $streams["stream"][$i] . "</h2>" .
				// Stream-Type and Description
				"<p><span style='color:#3fa8d2;margin-right:1em;'>" .
				$streams["game"][$i] .
				"</span>" .
				$streams["desc"][$i] .
				"</p>" .
				// Inner Container
				"<div>" .
				// Stream Preview
				"<img id='img-" . $streams["stream"][$i] . "' " .
				"class='preview' " .
				"src='" . $streams["img"][$i] . "'>" .
				// Button outer container
				"<div class='buttons-outer'>" .
				// Chat-Placeholder (for Embedding)
				/*"<div id='chat-placeholder-" .
				$streams["stream"][$i] .
				"'></div>" .*/
				// Button inner container
				"<div class='buttons-inner'>" .
				// Stream (external Player)
				"<a " .
				"class='button' " .
				// "target='_blank' " .
				"href='/?stream=" . rawurlencode($streams["stream"][$i]) .
				"' " .
				"title='External Player'>" .
				"<img " .
				"class='button-img' " .
				"src='/assets/img/play_mono.svg'>" .
				"</a>" .
				// Stream (Embedding)
				// "<a " .
				// "class='button' ".
				// "href='javascript:toggle_player(\"" .
				// $streams["stream"][$i] . "\", \"" . $streams["img"][$i] .
				// "\")' " .
				// "title='Player'>" .
				// "<img " .
				// "class='button-img' " .
				// "src='/assets/img/play_mono.svg'>" .
				// "</a>" .
				// Stream (New Tab)
				/*"<a " .
				"class='button' ".
				"target='_blank' ".
				"href='https://m.twitch.tv/" .
				$streams["stream"][$i] . "' " .
				"title='Twitch'>".
				"<img class='button-img' " .
				"src='/assets/img/twitch_mono.svg'>" .
				"</a>" .*/
				// Stream (Streamlink)
				"<a " .
				"class='button' " .
				"href='streamlink://twitch.tv/" .
				$streams["stream"][$i] .
				"?title=" .
				rawurlencode($streams["desc"][$i]) .
				"' title='Streamlink'>" .
				"<img class='button-img' " .
				"src='/assets/img/streamlink_mono.svg'>" .
				"</a>" .
				/* Chat (New Tab) */
				"<a ".
				"class='button' ".
				"target='_blank' ".
				"href='https://m.twitch.tv/popout/" .
				$streams["stream"][$i] .
				"/chat' " .
				"title='Chat'>" .
				"<img class='button-img' " .
				"src='/assets/img/chat_mono.svg'>" .
				"</a>" .
				/* Chat (Embedding) */
				/*"<a " .
				"class='button' ".
				"href='javascript:toggle_chat(\"" .
				$streams["stream"][$i] .
				"\")' " .
				"title='Chat'>" .
				"<img class='button-img' " .
				"src='/assets/img/chat_mono.svg'>" .
				"</a>" .*/
				/* Close inner button container */
				"</div>" .
				/* Close outer button container */
				"</div>" .
				/* Close Stream container */
				"</div>" .
				/* Divider */
				"<br><hr>";
		}
	}
	$content = "<ul class='container'>" .
		$content_menu .
		"</ul><br>" .
		$content;
} else if (!$extplayer) {
	// Generate Loading Screen items
	$menu_items = "<li>" .
		"<strong style='color:#fff;'>" .
		"Loading Twitch Streams " .
		"<span style='color:#ff0;' class='pulse'>&#8226;</span>" .
		"</strong>" .
		"</li>";
	$content = "<h1 style='text-align:center;'>Please be patient</h1>" .
		"<p style='text-align:center;'>Loading " .
		sizeof($streams["stream"]) .
		" streams. This may take a moment.</p>" .
		"<br>" .
		/* Loading Screen Animation */
		"<div class='wrapper-loading'>" .
		"<div class='box-wrap'>" .
		"<div class='box one'></div>" .
		"<div class='box two'></div>" .
		"<div class='box three'></div>" .
		"<div class='box four'></div>" .
		"<div class='box five'></div>" .
		"<div class='box six'></div>" .
		"</div>" .
		"</div>" .
		/* Streams-form */
		"<form id='channels' action='/' method='post'>" .
		"<input " .
		"type='hidden' " .
		"id='streams' " .
		"name='streams' ".
		"value='" . serialize($streams["stream"]) ."'" .
		">" .
		/* Form Submitter */
		"<script>" .
		"setTimeout(" .
		"function(){" .
		"document.querySelector('#channels').submit()" .
		"}, " .
		"1000);" .
		"</script>";
} else if ($extplayer) {
	$menu_items = "<li>Playing stream of " . $_GET["stream"] . "</li>";
	$content = "<div id='player'>" .
		"<div id='stream-window'>" .
		"<iframe id='stream' allowfullscreen scrolling='no' " .
		"src='https://player.twitch.tv/?channel=" . $_GET["stream"] .
		"&parent=" . $_SERVER['SERVER_NAME'] . "' " .
		"width='100%' height='100%'>" .
		"</iframe>" .
		"</div>" .
		"<div id='chat-window'>" .
		"<iframe scrolling='yes' " .
		"src='https://www.twitch.tv/embed/" . $_GET["stream"] .
		"/chat?parent=" . $_SERVER['SERVER_NAME'] . "'" .
		" width='100%' height='100%'>" .
		"</iframe>" .
		"</div>" .
		"</div>" .
		// FIXME: make chat full height
		"<script>setTimeout(function() { document.querySelector('#chat-window').style.height=document.querySelector('#stream-window').offsetHeight + 'px'; }, 1000);</script>";

} else {

	$menu_items = "<li><strong>Oh no!</strong></li>";
	$content = "<p>You tried to do something that is not implemented (yet?)...";
}
?>

<!-- HTML -->

<body>

	<!-- NAVIGATION BAR -->

	<nav id="nav">
	<ul class="container">

<?php
	/* Reload & Save Buttons */
	if (implode(",", $streams["stream"]) == $_COOKIE["streams"]) {
		$save_button_style = "display:none;";
	} else {
		$save_button_style = "";
	}
	echo "<li>" .
		// Save Button
		"<a " .
		"style='color:#F00;" . $save_button_style . "'".
		"href=\"#\"" .
		"onclick=\"document.cookie='streams=" .
		implode(",", $streams["stream"]) .
		";path=/;max-age=31536000;';\"" .
		"title='Save' " .
		"id='button-save'>" .
		"&#128427;" .
		"</a>" .
		"</li>" .
		"<li>" .
		// Reoad Button
		"<a " .
		"href=\"./?streams=" .
		implode(",", $streams["stream"]) .
		"\"" .
		"title='Reload' " .
		"id='button-reload'>" .
		"&#8635;" .
		"</a>" .
		"</li>";
	/* Other menu items */
	echo $menu_items;
?>
	</ul>
	</nav>

	<article class="wrapper">

	<!-- SITE CONTENT -->

<?php

	if (!$extplayer) {
		echo "<div class=\"container\">";
	}

	echo $content;

	if (!$extplayer) {
		echo "</div>";
	}
?>

	</article>
</body>
</html>
